This program is used to plumb historical cargo-vet info through to production,
so it can be displayed at go/cros-cargo-vet-dashboard. For more information on
how this happens, see the comments of b/279206748. For more information on the
program itself, please run `cargo run -- --help`.
